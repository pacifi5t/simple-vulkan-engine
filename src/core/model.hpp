#pragma once

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include "render/device.hpp"

#include <glm/glm.hpp>

namespace sve {
class Model {
  Device &device;
  VkBuffer vertexBuffer;
  VkDeviceMemory vertexBufferMemory;
  uint32_t vertexCount;

public:
  struct Vertex {
    glm::vec3 position;
    glm::vec3 color;
    static std::vector<VkVertexInputBindingDescription>
    getBindingDescriptions();
    static std::vector<VkVertexInputAttributeDescription>
    getAttributeDescriptions();
  };

  Model(Device &device, const std::vector<Vertex> &vertices);
  ~Model();

  void bind(VkCommandBuffer commandBuffer);
  void draw(VkCommandBuffer commandBuffer);

private:
  void createVertexBuffers(const std::vector<Vertex> &vertices);

  Model(const Model &) = delete;
  Model &operator=(const Model &) = delete;
};

} // namespace sve