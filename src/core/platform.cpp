#include "platform.hpp"

namespace sve {
Platform get_platform() {
  Platform p;

#ifdef _WIN32
  p = Platform::Windows;
#elif __APPLE__
  p = Platform::macOS;
#elif __linux__
  p = Platform::Linux;
#else
  p = Platform::unknown;
#endif

  return p;
}
} // namespace sve