#include "game_object.hpp"

sve::GameObject sve::GameObject::init() {
  static id_t currentId = 0;
  return GameObject{currentId++};
}

sve::id_t sve::GameObject::getId() const { return id; }