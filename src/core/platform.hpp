#pragma once
namespace sve {
enum Platform { Windows, Linux, macOS, unknown };

Platform get_platform();
} // namespace sve