#pragma once

#include "core/model.hpp"
#include "swap_chain.hpp"
#include "window.hpp"

#include <cassert>
#include <memory>
#include <vector>

namespace sve {
class Renderer {
  Window &window;
  Device &device;
  std::unique_ptr<SwapChain> swapChain;
  std::vector<VkCommandBuffer> commandBuffers;

  uint32_t currentImageIndex;
  int currentFrameIndex = 0;
  bool isFrameStarted = false;

  void createCommandBuffers();
  void freeCommandBuffers();
  void recreateSwapChain();

public:
  Renderer(Window &window, Device &device);

  ~Renderer();

  VkRenderPass getSwapChainRenderPass() const {
    return swapChain->getRenderPass();
  }

  bool isFrameInProgress() const { return isFrameStarted; }
  int getFrameIndex() const {
    assert(
        isFrameStarted && "Cannot get frame index when frame not in progress");
    return currentFrameIndex;
  }

  VkCommandBuffer getCurrentCommandBuffer() const {
    assert(
        isFrameStarted &&
        "Cannot get command buffer when frame not in progress");
    return commandBuffers[currentFrameIndex];
  }

  VkCommandBuffer beginFrame();
  void endFrame();
  void beginSwapChainRenderPass(VkCommandBuffer commandBuffer);
  void endSwapChainRenderPass(VkCommandBuffer commandBuffer);

  Renderer(const Renderer &) = delete;
  Renderer &operator=(const Renderer &) = delete;
};

} // namespace sve
