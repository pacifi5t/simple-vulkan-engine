#pragma once

#include "core/game_object.hpp"
#include "core/model.hpp"
#include "renderer.hpp"
#include "window.hpp"

#include <memory>
#include <vector>

namespace sve {
class Engine {
  Window window{WIDTH, HEIGHT, "VULKAN!"};
  Device device{window};
  Renderer renderer{window, device};
  std::vector<GameObject> gameObjects;

  void loadGameObjects();

public:
  static const int WIDTH = 800;
  static const int HEIGHT = 600;

  Engine();

  ~Engine();

  void run();

  Engine(const Engine &) = delete;
  Engine &operator=(const Engine &) = delete;
};

} // namespace sve