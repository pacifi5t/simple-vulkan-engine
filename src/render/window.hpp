#pragma once

#define GLFW_INCLUDE_VULKAN

#include <GLFW/glfw3.h>
#include <string>

namespace sve {
class Window {
private:
  int width;
  int height;
  bool frameBufferResized = false;
  std::string windowName;
  GLFWwindow *window;

  void init_window();
  static void frameBufferResizeCallback(GLFWwindow *window, int width, int height);

public:
  Window(int w, int h, std::string name);

  ~Window();

  bool shouldClose();

  VkExtent2D getExtent() {
    return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};
  }

  bool wasWindowResized() {
    return frameBufferResized;
  }

  void resetWindowResizeFlag() {
    frameBufferResized = false;
  }

  void createWindowSurface(VkInstance instance, VkSurfaceKHR *surface);

  Window(const Window &) = delete;

  Window &operator=(const Window &) = delete;
};
} // namespace sve
