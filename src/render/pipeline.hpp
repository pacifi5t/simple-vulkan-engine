#pragma once

#include "device.hpp"

#include <string>
#include <vector>

namespace sve {
struct PipelineConfigInfo {
  PipelineConfigInfo(const PipelineConfigInfo &) = delete;
  PipelineConfigInfo &operator=(const PipelineConfigInfo &) = delete;

  VkPipelineViewportStateCreateInfo viewportInfo;
  VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
  VkPipelineRasterizationStateCreateInfo rasterizationInfo;
  VkPipelineMultisampleStateCreateInfo multisampleInfo;
  VkPipelineColorBlendAttachmentState colorBlendAttachment;
  VkPipelineColorBlendStateCreateInfo colorBlendInfo;
  VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
  std::vector<VkDynamicState> dynamicStateEnables;
  VkPipelineDynamicStateCreateInfo dynamicStateInfo;
  VkPipelineLayout pipelineLayout = nullptr;
  VkRenderPass renderPass = nullptr;
  uint32_t subpass = 0;
};

class Pipeline {
  Device &device;
  VkPipeline graphicsPipeline;
  VkShaderModule vertShaderModule;
  VkShaderModule fragShaderModule;

public:
  Pipeline(
      Device &device,
      const std::string &vertPath,
      const std::string &fragPath,
      const PipelineConfigInfo &configInfo);

  ~Pipeline();

  static void defaultPipelineConfigInfo(PipelineConfigInfo &configInfo);

  void bind(VkCommandBuffer commandBuffer);

  Pipeline(const Pipeline &) = delete;
  Pipeline operator=(const Pipeline &) = delete;

private:
  static std::vector<char> readFile(const std::string &filepath);

  void createGraphicsPipeline(
      const std::string &vert_path,
      const std::string &frag_path,
      const PipelineConfigInfo &config);

  void createShaderModule(
      const std::vector<char> &code,
      VkShaderModule *shaderModule);
};
} // namespace sve