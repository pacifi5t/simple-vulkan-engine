#include "window.hpp"

#include <stdexcept>

namespace sve {
Window::Window(int w, int h, std::string name)
    : width{w}, height{h}, windowName{name} {
  init_window();
}

Window::~Window() {
  glfwDestroyWindow(window);
  glfwTerminate();
}

void Window::init_window() {
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

  window =
      glfwCreateWindow(width, height, windowName.c_str(), nullptr, nullptr);
      glfwSetWindowUserPointer(window, this);
      glfwSetFramebufferSizeCallback(window, frameBufferResizeCallback);
}

bool Window::shouldClose() { return glfwWindowShouldClose(window); }

void Window::createWindowSurface(
    VkInstance instance,
    VkSurfaceKHR *surface) {
  if (glfwCreateWindowSurface(instance, window, nullptr, surface) !=
      VK_SUCCESS) {
    throw std::runtime_error("Failed to create window surface");
  }
}

void Window::frameBufferResizeCallback(GLFWwindow *window, int width, int height) {
  auto sveWindow = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  sveWindow->frameBufferResized = true;
  sveWindow->width = width;
  sveWindow->height = height;
}
} // namespace sve