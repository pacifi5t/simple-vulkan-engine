#pragma once

#include "core/game_object.hpp"
#include "core/model.hpp"
#include "pipeline.hpp"

#include <memory>
#include <vector>

namespace sve {
class SimpleRenderSystem {
  Device &device;

  std::unique_ptr<Pipeline> pipeline;
  VkPipelineLayout pipelineLayout;

  void createPipelineLayout();
  void createPipeline(VkRenderPass renderPass);

public:
  SimpleRenderSystem(Device &device, VkRenderPass renderPass);

  ~SimpleRenderSystem();

  void renderGameObjects(
      VkCommandBuffer commandBuffer,
      std::vector<GameObject>& gameObjects);

  SimpleRenderSystem(const SimpleRenderSystem &) = delete;
  SimpleRenderSystem &operator=(const SimpleRenderSystem &) = delete;
};

} // namespace sve