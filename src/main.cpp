#include "core/platform.hpp"
#include "render/engine.hpp"

#include <iostream>
#include <stdexcept>

using sve::Platform;

std::string BASE_PATH;
Platform platform;

std::string get_executable_dir(char *argv0) {
  std::string str(realpath(argv0, nullptr));
  size_t i = str.length();

  while (str[i - 1] != '/') {
    i--;
  }
  return str.erase(i, str.length() - i);
}

void print_platform() {
  std::string os = "unknown";

  if (platform == Platform::Windows) {
    os = "Windows";
  } else if (platform == Platform::Linux) {
    os = "Linux";
  } else if (platform == Platform::macOS) {
    os = "macOS";
  }

  std::cout << "OS: " << os << std::endl;
}

int main(int argc, char **argv) {
  BASE_PATH = get_executable_dir(argv[0]);
  platform = sve::get_platform();
  print_platform();

  sve::Engine engine;

  try {
    engine.run();
  } catch (const std::exception &e) {
    std::cerr << "Engine error: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
